# iStore

https://gitlab.com/SamiraTesse/2java-project

Repository du projet iStore. Ce dépot héberge les différents dossiers du projet contenant les fonctionnalités de ce dernier.

## Installation des outils

Note: Vous devrez au préalable installer un IDE(Integrated Development Environment) pour Java.
Nous vous recommendons IntelliJ, mais vous êtes libre de prendre celui de votre choix.

Les IDEs sont des logiciels premettant de développer des applications dans les différents language de programmation.

Installation et démarrage en ligne de commandes de Docker Desktop pour l'utilisation des services de gestion base de données (MYSQL & PhpMyadmin).

**-Pour Windows:**

```shell
#After downloading Docker Desktop Installer.exe, run the following command in a terminal to install Docker Desktop:
"Docker Desktop Installer.exe" install

#If you’re using PowerShell you should run it as:
Start-Process 'Docker Desktop Installer.exe' -Wait install

#If using the Windows Command Prompt:
start /w "" "Docker Desktop Installer.exe" install
```
**-Pour Linux:**

Pré-requis:

Pour installer Docker Desktop avec succès, vous devez :

Répondre aux exigences du système
Avoir une version 64 bits de la dernière version LTS (Ubuntu Jammy Jellyfish 22.04) ou de la version actuelle non LTS (Ubuntu Mantic Minotaur 23.10). Docker Desktop est pris en charge sur l'architecture x86_64 (ou amd64).
Pour les environnements autres que Gnome Desktop, gnome-terminal doit être installé:
```shell
sudo apt install gnome-terminal
```

Approche recommandée pour installer Docker Desktop sur Ubuntu:

Configurez le référentiel de packages de Docker. Voir la première étape de l'installation à l'aide du référentiel apt.

Téléchargez le dernier package DEB.

Installez le package avec apt comme suit:

```shell
sudo apt-get update

sudo apt-get install ./docker-desktop-<version>-<arch>.deb

systemctl --user start docker-desktop
```

**-Pour MacOS:**

```shell
sudo hdiutil attach Docker.dmg

sudo /Volumes/Docker/Docker.app/Contents/MacOS/install

sudo hdiutil detach /Volumes/Docker
```

## Clonage du dépôt distant

Une fois docker desktop installé sur votre ordinateur, vous pouvez clôner votre dépôt distant sur dans votre environnement local à l'aide des commandes Git dans le dossier de votre choix:

```shell
cd existing_repo

git clone https://gitlab.com/SamiraTesse/2java-project.git
```

## Démarrage du conteneur

Une fois votre projet disponible en local sur votre ordinateur, procédez au démarrage du conteneur contenant les services Mysql et Phpmyadmin à l'aide du fichier docker-compose.yml.

```shell
cd 2java-project/iStore/devcontainer/

docker compose up
```
## Importation de la  base de données

Avant de vous connecter à Phpmyadmin avec les identifiants fournis dans la classe **DatabaseHandler.java** du projet, téléchargez le fichier mysql contenant les différentes tables de notre base de données iStore.

https://gitlab.com/SamiraTesse/2java-project/-/raw/main/iStore/src/main/java/com/supinfo/iStore/infrastructure/database/iStore.sql

Puis, identifiez-vous sur phpmyadmin et importez le fichier istore dans la nouvelle base de données **istore** que vous aurez au préalable créé.

## Démarrage de l'iStore

À présent, vous pouvez lancer l'application en cliquant sur le bouton Run dans le fichier **Main.java**.

Afin the vous connecter à l'application, utilisez les identifiants par défauts:
- Email: john.doe@admin.com
- Password: password

### Algorithme de l'application (simplifiée)
![2java_process_tree.drawio.svg](2java_process_tree.drawio.svg)
### Diagramme de Classe
![Diagram_ERD.jpg](Diagram_ERD.jpg)