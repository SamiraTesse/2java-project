package com.supinfo.iStore.useCases;

import com.supinfo.iStore.usecase.SignupUseCase;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

public class SignupUseCaseTest {

    private final String testEmail = "newuser@example.com";
    private final String testUsername = "testuser";
    private final String testName = "Test";
    private final String testSurname = "User";
    private final String testDOB = "1990-01-01";
    private final String testRole = "user";
    private final String testPassword = "password123";

    @BeforeEach
    public void setUp() {
        // Delete the example user before each test
        deleteUser(testEmail);
    }

    @AfterEach
    public void tearDown() {
        // Delete the example user after each test
        deleteUser(testEmail);
    }

    @Test
    public void testSignUp_Successful() {
        // Test sign up with a new user
        boolean signedUp = SignupUseCase.signUp(testUsername, testName, testSurname, testDOB, testEmail, testPassword, testRole);
        assertTrue(signedUp, "First sign up should be successful");
    }

    @Test
    public void testSignUp_UserAlreadyExists() {
        // First, sign up the user
        boolean firstSignUp = SignupUseCase.signUp(testUsername, testName, testSurname, testDOB, testEmail, testPassword, testRole);
        assertTrue(firstSignUp, "First sign up should be successful");

        // Then, try to sign up again with the same email
        boolean secondSignUp = SignupUseCase.signUp(testUsername, testName, testSurname, testDOB, testEmail, testPassword, testRole);
        assertFalse(secondSignUp, "Second sign up should fail because the user already exists");
    }

    private void deleteUser(String email) {
        // Assuming the database connection details
        String url = "jdbc:mysql://localhost:3306/istore";
        String username = "root";
        String dbPassword = "";

        try (Connection connection = DriverManager.getConnection(url, username, dbPassword)) {
            // Delete user from the 'identifiants' table
            String deleteQuery = "DELETE FROM identifiants WHERE email = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery)) {
                preparedStatement.setString(1, email);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException ex) {
            // Handle any SQL errors
            fail("Failed to delete user: " + ex.getMessage());
        }
    }
}
