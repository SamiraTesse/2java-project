package com.supinfo.iStore.useCases;

import com.supinfo.iStore.usecase.employees.EmployeeDisplayInfoUseCase;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.sql.SQLException;


import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserInfoUseCaseTest {

    @Test
    public void testDisplayOtherUsers() throws SQLException {
        // Redirect System.out to capture console output
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outputStream));

        // Test the displayOtherUsers method
        EmployeeDisplayInfoUseCase.display();

        // Restore System.out
        System.setOut(originalOut);

        // Check if the output contains expected information
        String consoleOutput = outputStream.toString();
        assertTrue(consoleOutput.contains("Full Name"));
        assertTrue(consoleOutput.contains("Phone Number"));
    }

    // Additional test cases can be added here if needed
}
