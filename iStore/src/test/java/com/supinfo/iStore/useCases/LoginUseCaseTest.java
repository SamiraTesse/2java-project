package com.supinfo.iStore.useCases;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import com.supinfo.iStore.usecase.LoginUseCase;

public class LoginUseCaseTest {

    @Test
    public void testLogin_Successful() {
        String email = "test@example.com";
        String password = "password123";

        boolean loggedIn = LoginUseCase.login(email, password);
        assertTrue(loggedIn);
    }

    @Test
    public void testLogin_IncorrectPassword() {
        String email = "test@example.com";
        String password = "wrongpassword";

        boolean loggedIn = LoginUseCase.login(email, password);
        assertFalse(loggedIn);
    }

    @Test
    public void testLogin_UserNotFound() {
        String email = "nonexistent@example.com";
        String password = "password123";

        boolean loggedIn = LoginUseCase.login(email, password);
        assertFalse(loggedIn);
    }
}
