package com.supinfo.iStore.useCases;

import com.supinfo.iStore.usecase.UserDeleteProfileUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.*;

import static org.junit.jupiter.api.Assertions.*;

public class UserDeleteProfileUseCaseTest {

    private static final String TEST_EMAIL = "test@example.com";
    private static final String TEST_PASSWORD = "password123";

    @BeforeEach
    public void setup() {
        String hashedPassword = BCrypt.hashpw(TEST_PASSWORD, BCrypt.gensalt());
        insertDummyUser(TEST_EMAIL, hashedPassword);
    }

    private void insertDummyUser(String email, String hashedPassword) {
        String url = "jdbc:mysql://localhost:3306/istore";
        String usernameDB = "root";
        String dbPassword = "";

        try (Connection connection = DriverManager.getConnection(url, usernameDB, dbPassword)) {
            String insertQuery = "INSERT INTO identifiants (email, password) VALUES (?, ?)";
            try (PreparedStatement statement = connection.prepareStatement(insertQuery)) {
                statement.setString(1, email);
                statement.setString(2, hashedPassword);
                statement.executeUpdate();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            fail("Failed to insert dummy user into the database");
        }
    }

    @Test
    public void testDeleteProfile_SuccessfulDeletion() {
        String email = TEST_EMAIL;
        String password = TEST_PASSWORD;

        boolean deletionResult = UserDeleteProfileUseCase.deleteProfile(email, password);

        assertTrue(deletionResult, "Deletion should be successful");

        assertFalse(isUserExists(email), "User profile should be deleted");
    }

    private boolean isUserExists(String email) {
        String url = "jdbc:mysql://localhost:3306/istore";
        String usernameDB = "root";
        String dbPassword = "";

        try (Connection connection = DriverManager.getConnection(url, usernameDB, dbPassword)) {
            String query = "SELECT COUNT(*) FROM identifiants WHERE email = ?";
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, email);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        int count = resultSet.getInt(1);
                        return count > 0;
                    }
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
