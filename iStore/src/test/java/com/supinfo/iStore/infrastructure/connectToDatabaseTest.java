package com.supinfo.iStore.infrastructure;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class connectToDatabaseTest {

    @Test
    public void shouldCheckDatabaseExists() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "istore";
        String username = "root";
        String password = "";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             Statement statement = connection.createStatement()) {

            boolean databaseExists = statement.execute("SHOW DATABASES LIKE '" + dbName + "'");

            Assertions.assertTrue(databaseExists);
        }
    }

    @Test
    public void shouldCheckIfCanConnect() {
        String url = "jdbc:mysql://localhost:3306/istore";
        String username = "root";
        String password = "";

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            Assertions.assertNotNull(connection);
        } catch (SQLException e) {
            Assertions.assertNull(e.getMessage());
        }
    }
}
