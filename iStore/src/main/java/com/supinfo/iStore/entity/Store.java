package com.supinfo.iStore.entity;

import com.supinfo.iStore.infrastructure.employee.AddEmployeeToStore;
import com.supinfo.iStore.infrastructure.store.StoreCreation;

import java.sql.*;

public class Store {
    public static boolean isStoreExists(Connection connection, String storeName) throws SQLException {
        String query = StoreCreation.getSelectQuery();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, storeName);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    int count = resultSet.getInt(1);
                    return count > 0;
                }
            }
        }
        return false;
    }

    public static int getStoreIdByName(Connection connection, String storeName) {
        String query = AddEmployeeToStore.getSelectStoreIdQuery();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, storeName);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt("id");
                } else {
                    return -1;
                }
            }
        } catch (SQLException ex) {
            System.err.println("Error retrieving store ID: " + ex.getMessage());
            return -1;
        }
    }

}
