package com.supinfo.iStore.entity;

import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.employee.Employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeAccount{
    public static String getEmployeeEmail(int updateId) throws SQLException {
        String query = Employee.getEmailByIdQuery();
        try (Connection connection = DatabaseHandler.establishConnection()) {
            assert connection != null;
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, updateId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next())
                        return resultSet.getString("email");
                    else return null;
                }
            }
        }
    }
}
