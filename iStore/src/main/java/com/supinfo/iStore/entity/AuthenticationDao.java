package com.supinfo.iStore.entity;

import com.supinfo.iStore.usecase.LoginFailedException;

public interface AuthenticationDao {

    Account login(String mail, String password) throws LoginFailedException;
}
