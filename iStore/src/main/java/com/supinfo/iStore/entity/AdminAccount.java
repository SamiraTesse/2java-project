package com.supinfo.iStore.entity;


import com.supinfo.iStore.usecase.admins.employeeManagement.UpdateEmployeeAccountUseCase;

import java.sql.SQLException;
import java.util.Scanner;

public class AdminAccount{
    public static void updateEmployeeProfile(Scanner scanner) throws SQLException {
        System.out.println("Update Employee Profile:");
        System.out.print("Enter ID: ");
        int id = scanner.nextInt();
        scanner.nextLine();
        String email = EmployeeAccount.getEmployeeEmail(id);
        System.out.println(email);
        System.out.print("Enter the field you want to update (name/surname/dob/email/password): ");
        String fieldToUpdate = scanner.nextLine();
        System.out.print("Enter the new value: ");
        String newValue = scanner.nextLine();
        Account.updateProfile(email, fieldToUpdate, newValue);
        UpdateEmployeeAccountUseCase.updateEmployeeProfile(email, fieldToUpdate, newValue);
    }
}
