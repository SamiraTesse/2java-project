package com.supinfo.iStore.entity;

import com.supinfo.iStore.infrastructure.account.AccountCreation;
import com.supinfo.iStore.infrastructure.account.Authentication;
import com.supinfo.iStore.infrastructure.account.UpdateProfile;
import com.supinfo.iStore.infrastructure.database.DatabaseHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Account {

    public static boolean isAdmin(String email) {
        try (Connection connection = DatabaseHandler.establishConnection()){
            String role = getRole(email, connection);
            if (role == null) return false;
            return role.equalsIgnoreCase("admin");

        } catch (SQLException e) {
            System.err.println("Unable to connect to the Database.");
            return false;
        }
    }

    private static String getRole(String email, Connection connection) throws SQLException {
        if (connection == null) {
            System.err.println("Error 404. Something went wrong!");
            return null;
        }

        String hashedPassword = getHashedPassword(connection, email);
        String role = getRole(connection, email, hashedPassword);
        if (role == null) {
            System.err.println("An error occurred while checking user's infos.");
            return null;
        }
        return role;
    }

    public static boolean userExists(Connection connection, String email) throws SQLException {
        String query = AccountCreation.getSelectCredentialsQuery();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, email);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    int count = resultSet.getInt(1);
                    return count > 0;
                }
            }
        }

        return false;
    }

    public static boolean isWaiting(String email) {
        String query = Authentication.getWaitingListInfosQuery();

        try (Connection connection = DatabaseHandler.establishConnection()) {
            assert connection != null;
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {

                preparedStatement.setString(1, email);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if(resultSet.next()) {
                        int count = resultSet.getInt(1);
                        return count > 0;
                    }
                }
            }
        } catch (SQLException ex) {
            System.err.println("Error checking waiting list: " + ex.getMessage());
        }
        return false;
    }


    public static String getHashedPassword(Connection connection, String email) throws SQLException {
        String query = Authentication.getSelectPasswordQuery();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, email);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getString("password");
                } else {
                    return null;
                }
            }
        }
    }

    public static String getRole(Connection connection, String email, String password) throws SQLException {

        String query = Authentication.getSelectRoleQuery();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getString("role");
                } else {
                    return null;
                }
            }
        }
    }

    public static void updateProfile(String email, String fieldToUpdate, String newValue) {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            if (connection == null) {
                System.err.println("Error 404. Something went wrong!");
                return;
            }

            String updateWhitelistQuery = UpdateProfile.getUpdateWhitelistQuery(fieldToUpdate);
            String updateIdentifiantsQuery = UpdateProfile.getUpdateIdentifiantsQuery(fieldToUpdate);

            // Check if the field to update exists in both tables
            boolean fieldExistsInIdentifiants = fieldExistsInTable(connection, fieldToUpdate, "identifiants");
            boolean fieldExistsInWhitelist = fieldExistsInTable(connection, fieldToUpdate, "whitelist");

            if (fieldExistsInIdentifiants && fieldExistsInWhitelist) {
                boolean whitelistUpdated = false;

                try (PreparedStatement preparedStatement = connection.prepareStatement(updateWhitelistQuery)) {
                    preparedStatement.setString(1, newValue);
                    preparedStatement.setString(2, email);

                    int rowsAffected = preparedStatement.executeUpdate();

                    if (rowsAffected > 0) {
                        System.out.println("Profile updated successfully in whitelist table!");
                        whitelistUpdated = true;
                    } else {
                        System.out.println("Failed to update profile in whitelist table.");
                    }
                }

                if (whitelistUpdated) {
                    try (PreparedStatement preparedStatement = connection.prepareStatement(updateIdentifiantsQuery)) {
                        preparedStatement.setString(1, newValue);
                        preparedStatement.setString(2, email);

                        int rowsAffected = preparedStatement.executeUpdate();

                        if (rowsAffected > 0) {
                            System.out.println("Profile updated successfully in identifiants table!");
                        } else {
                            System.out.println("Failed to update profile in identifiants table.");
                        }
                    }
                }
            } else if (fieldExistsInWhitelist) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(updateWhitelistQuery)) {
                    preparedStatement.setString(1, newValue);
                    preparedStatement.setString(2, email);

                    int rowsAffected = preparedStatement.executeUpdate();

                    if (rowsAffected > 0) {
                        System.out.println("Profile updated successfully in whitelist table!");
                    } else {
                        System.out.println("Failed to update profile in whitelist table.");
                    }
                }
            } else {
                System.out.println("The field '" + fieldToUpdate + "' does not exist in both tables.");
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage() + '.');
        }
    }

    public static boolean fieldExistsInTable(Connection connection, String field, String tableName) {
        String query = UpdateProfile.getCheckExistanceQuery(field, tableName);

        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return resultSet.next();
            }
        } catch (SQLException e) {
            System.err.println("Error checking if field exists in table: " + e.getMessage());
            return false;
        }
    }



}

