-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql:3306
-- Generation Time: Feb 21, 2024 at 09:35 PM
-- Server version: 8.3.0
-- PHP Version: 8.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iStore`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int NOT NULL,
  `name` varchar(26) NOT NULL,
  `surname` varchar(26) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(26) NOT NULL,
  `password` varchar(255) NOT NULL,
  `store_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `surname`, `dob`, `email`, `password`, `store_id`) VALUES
(3, 'First', 'First_Employee', '2000-01-01', 'first.employee@istore.com', '$2a$10$q4y80RofOzJUEsTm1fwR1ee1rVyKhXCXfgcJCU9yoouCRAff9tD/C', 3);

-- --------------------------------------------------------

--
-- Table structure for table `identifiants`
--

CREATE TABLE `identifiants` (
  `ID` int NOT NULL,
  `email` varchar(26) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `identifiants`
--

INSERT INTO `identifiants` (`ID`, `email`, `password`) VALUES
(1, 'john.doe@admin.com', '$2a$10$gnm40CIerMN4AbwoRe.oTOi6HKEimsRJyFTc/ebe9XJ8Y1J537kIu'),
(2, 'second.admin@admin.com', '$2a$10$2v/E.KLQ99QPle1TiOBASe8Iy7jJ9OXFy/UFIFPrher72ibTVfbyS'),
(3, 'user.user@email.com', '$2a$10$6Myr1P1.Yf6coATXS3BycuGx/rPS8SowmU3Avl8ZgktJ/3Y527UXm'),
(4, 'something@something.com', '$2a$10$bI7tNLV6Ng0MvRezrDnOSesG1iAhDdTbWRGVbxqe5S7u8SEfJri0a'),
(6, 'first.employee@istore.com', '$2a$10$q4y80RofOzJUEsTm1fwR1ee1rVyKhXCXfgcJCU9yoouCRAff9tD/C'),
(7, 'dvrk@unicorn.com', '$2a$10$TW.I4w/ntvyO67Ir2ueR/eWfzG2Rz3j9UBzSrAZFh87CBPVpEFLcW');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `ID` int NOT NULL,
  `name` varchar(26) NOT NULL,
  `store_id` int DEFAULT NULL,
  `item_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`ID`, `name`, `store_id`, `item_id`) VALUES
(1, 'testInventory', 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int NOT NULL,
  `name` varchar(26) NOT NULL,
  `price` int NOT NULL,
  `inventory_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `price`, `inventory_id`) VALUES
(2, 'Samira', 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `id` int NOT NULL,
  `name` varchar(26) NOT NULL,
  `inventory_id` int DEFAULT NULL,
  `employee_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`id`, `name`, `inventory_id`, `employee_id`) VALUES
(3, 'UnicornWorld', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `waitinglist`
--

CREATE TABLE `waitinglist` (
  `ID` int NOT NULL,
  `username` varchar(26) NOT NULL,
  `Name` varchar(26) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Surname` varchar(26) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DOB` date NOT NULL,
  `Email` varchar(255) NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `role` varchar(26) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='A table containing all iStore users.';

--
-- Dumping data for table `waitinglist`
--

INSERT INTO `waitinglist` (`ID`, `username`, `Name`, `Surname`, `DOB`, `Email`, `password`, `role`) VALUES
(4, 'Test', 'Test', 'Test', '2000-02-05', 'email@email.com', '$2a$10$X7CinSAmUkmqmG7DT17n1.W6CmHTZdImIWP5G4dUI7Q9pljMferSO', 'Admin'),
(9, 'Second_Employee', 'Second', 'Employee', '2000-01-01', 'second.employee@istore.com', '$2a$10$gcMD0Ny2/w6VY2yR2V8atu6gtuHhQ9FxReBAudWxoIvomqruuQXsC', 'Employee'),
(10, 'Third_Employee', 'Third', 'Employee', '2000-01-01', 'third.employee@istore.com', '$2a$10$C5CAp6mk0Zz/LpDT0n0AD.K9wsZ7BXlbwp0IgFTSYecz5vAj8ftey', 'Employee');

-- --------------------------------------------------------

--
-- Table structure for table `whitelist`
--

CREATE TABLE `whitelist` (
  `id` int NOT NULL,
  `Username` varchar(26) NOT NULL,
  `Name` varchar(26) NOT NULL,
  `Surname` varchar(26) NOT NULL,
  `DOB` date NOT NULL,
  `email` varchar(26) NOT NULL,
  `password` varchar(255) NOT NULL,
  `Role` varchar(26) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `whitelist`
--

INSERT INTO `whitelist` (`id`, `Username`, `Name`, `Surname`, `DOB`, `email`, `password`, `Role`) VALUES
(2, 'SuperAdmin', 'John', 'Doe', '1999-01-01', 'john.doe@admin.com', '$2a$10$gnm40CIerMN4AbwoRe.oTOi6HKEimsRJyFTc/ebe9XJ8Y1J537kIu', 'Admin'),
(3, 'Admin', 'Second ', 'Overseer', '2000-01-05', 'second.admin@admin.com', '$2a$10$2v/E.KLQ99QPle1TiOBASe8Iy7jJ9OXFy/UFIFPrher72ibTVfbyS', 'Admin'),
(4, 'User', 'User', 'User', '1999-10-10', 'user.user@email.com', '$2a$10$6Myr1P1.Yf6coATXS3BycuGx/rPS8SowmU3Avl8ZgktJ/3Y527UXm', 'User'),
(5, 'newUser', 'newUser', 'newUser', '2000-10-10', 'something@something.com', '$2a$10$bI7tNLV6Ng0MvRezrDnOSesG1iAhDdTbWRGVbxqe5S7u8SEfJri0a', 'User'),
(7, 'First_Employee', 'caca', 'caca', '2000-01-01', 'first.employee@istore.com', '$2a$10$q4y80RofOzJUEsTm1fwR1ee1rVyKhXCXfgcJCU9yoouCRAff9tD/C', 'Employee'),
(8, 'Dvrkunicorn', 'caca', 'Unicorn', '2002-03-09', 'dvrk@unicorn.com', '$2a$10$TW.I4w/ntvyO67Ir2ueR/eWfzG2Rz3j9UBzSrAZFh87CBPVpEFLcW', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_store_id_fk` (`store_id`);

--
-- Indexes for table `identifiants`
--
ALTER TABLE `identifiants`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `inventory_store_id_fk` (`store_id`),
  ADD KEY `inventory_items_id_fk` (`item_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_inventory_ID_fk` (`inventory_id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`),
  ADD KEY `store_inventory_ID_fk` (`inventory_id`),
  ADD KEY `store_employee_id_fk` (`employee_id`);

--
-- Indexes for table `waitinglist`
--
ALTER TABLE `waitinglist`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `whitelist`
--
ALTER TABLE `whitelist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `identifiants`
--
ALTER TABLE `identifiants`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `waitinglist`
--
ALTER TABLE `waitinglist`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `whitelist`
--
ALTER TABLE `whitelist`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_store_id_fk` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`);

--
-- Constraints for table `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `inventory_items_id_fk` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `inventory_store_id_fk` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`);

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_inventory_ID_fk` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`ID`);

--
-- Constraints for table `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `store_employee_id_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`),
  ADD CONSTRAINT `store_inventory_ID_fk` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
