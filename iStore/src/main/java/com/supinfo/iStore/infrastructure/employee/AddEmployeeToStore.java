package com.supinfo.iStore.infrastructure.employee;

public class AddEmployeeToStore {
    public static String getSelectStoreIdQuery() {
        return "SELECT id FROM store WHERE name = ?";
    }
//    public static String getInsertEmployeeQuery() {
//        return "INSERT INTO store_employee (employee_name, store_id) VALUES (?, ?)";
//    }

    public static String getAddEmployeeQuery() {
        return "UPDATE store SET employee_id = ? WHERE name = ?";
    }

    public static String getUpdateEmployeeQuery() {
        return "UPDATE employee SET store_id = ? WHERE id = ?";
    }

}
