package com.supinfo.iStore.infrastructure.database;

public class Migrations {

    private static final String AUTO_INCREMENT_PRIMARY_KEY = "id int auto_increment primary key";
    private static final String VARCHAR_26_NOT_NULL = "varchar(26) not null";
    private static final String VARCHAR_255_NOT_NULL = "varchar(255) not null";
    private static final String DATE_NOT_NULL = "date not null";

    private static final String FOREIGN_KEY_REFERENCE_STORE = "foreign key (store_id) references store (id)";
    private static final String FOREIGN_KEY_REFERENCE_INVENTORY = "foreign key (inventory_id) references inventory (id)";

    public static String createIdentifiantsTable() {
        return "create table istore.identifiants (\n" +
                "    " + AUTO_INCREMENT_PRIMARY_KEY + ",\n" +
                "    email " + VARCHAR_26_NOT_NULL + ",\n" +
                "    password " + VARCHAR_255_NOT_NULL + "\n" +
                ");\n";
    }

    public static String createInventoryTable() {
        return "create table iStore.inventory (\n" +
                "    ID int auto_increment primary key,\n" +
                "    name " + VARCHAR_26_NOT_NULL + ",\n" +
                "    store_id int null,\n" +
                "    item_id int null,\n" +
                "    constraint inventory_items_id_fk foreign key (item_id) references iStore.items (id),\n" +
                "    constraint inventory_store_id_fk foreign key (store_id) references iStore.store (id)\n" +
                ");\n\n";
    }

    public static String createItemsTable() {
        return "create table istore.items (\n" +
                "    " + AUTO_INCREMENT_PRIMARY_KEY + ",\n" +
                "    name " + VARCHAR_26_NOT_NULL + ",\n" +
                "    price int not null,\n" +
                "    inventory_id int not null,\n" +
                "    constraint items_inventory_ID_fk " + FOREIGN_KEY_REFERENCE_INVENTORY + "\n" +
                ");\n";
    }

    public static String createStoreTable() {
        return "create table istore.store (\n" +
                "    " + AUTO_INCREMENT_PRIMARY_KEY + ",\n" +
                "    name " + VARCHAR_26_NOT_NULL + ",\n" +
                "    inventory_id int null,\n" +
                "    employee_id int null,\n" +
                "    constraint store_employee_id_fk " + FOREIGN_KEY_REFERENCE_STORE + ",\n" +
                "    constraint store_inventory_ID_fk " + FOREIGN_KEY_REFERENCE_INVENTORY + "\n" +
                ");\n";
    }

    public static String createWaitingListTable() {
        return "create table istore.waitinglist (\n" +
                "    " + AUTO_INCREMENT_PRIMARY_KEY + ",\n" +
                "    username " + VARCHAR_26_NOT_NULL + ",\n" +
                "    name " + VARCHAR_26_NOT_NULL + ",\n" +
                "    surname " + VARCHAR_26_NOT_NULL + ",\n" +
                "    dob " + DATE_NOT_NULL + ",\n" +
                "    email " + VARCHAR_255_NOT_NULL + ",\n" +
                "    password " + VARCHAR_255_NOT_NULL + ",\n" +
                "    role " + VARCHAR_26_NOT_NULL + " default 'user' not null\n" +
                ");\n";
    }

    public static String createWhitelistTable() {
        return "create table istore.whitelist (\n" +
                "    " + AUTO_INCREMENT_PRIMARY_KEY + ",\n" +
                "    username " + VARCHAR_26_NOT_NULL + ",\n" +
                "    name " + VARCHAR_26_NOT_NULL + ",\n" +
                "    surname " + VARCHAR_26_NOT_NULL + ",\n" +
                "    dob " + DATE_NOT_NULL + ",\n" +
                "    email " + VARCHAR_26_NOT_NULL + ",\n" +
                "    password " + VARCHAR_255_NOT_NULL + ",\n" +
                "    role " + VARCHAR_26_NOT_NULL + " not null\n" +
                ");\n";
    }

    public static String createEmployeeTable() {
        return "create table istore.employee (\n" +
                "    " + AUTO_INCREMENT_PRIMARY_KEY + ",\n" +
                "    name " + VARCHAR_26_NOT_NULL + ",\n" +
                "    surname " + VARCHAR_26_NOT_NULL + ",\n" +
                "    dob " + DATE_NOT_NULL + ",\n" +
                "    email " + VARCHAR_26_NOT_NULL + ",\n" +
                "    password " + VARCHAR_255_NOT_NULL + ",\n" +
                "    store_id int null,\n" +
                "    constraint employee_store_id_fk " + FOREIGN_KEY_REFERENCE_STORE + "\n" +
                ");\n";
    }
}
