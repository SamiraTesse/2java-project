package com.supinfo.iStore.infrastructure.store;

public class StoreAccessList {
    public static String getSelectEmployeesQuery() {
        return "SELECT * FROM employee WHERE store_id = ?";
    }


    public static String getStoreInfoQuery() {
        return "SELECT * FROM store WHERE id = ?";
    }

    public static String getStoreIdQuery() {
        return "SELECT store_id FROM employee WHERE store_id IS NOT NULL AND store_id > 0";
    }
}
