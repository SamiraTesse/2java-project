package com.supinfo.iStore.infrastructure.account;

public class UpdateProfile {
    public static String getUpdateWhitelistQuery(String fieldToUpdate) {
        return "UPDATE whitelist SET " + fieldToUpdate + " = ? WHERE email = ?";
    }
    public static String getUpdateIdentifiantsQuery(String fieldToUpdate) {
        return "UPDATE identifiants SET " + fieldToUpdate + " = ? WHERE email = ?";
    }

    public static String getCheckExistanceQuery(String field, String tableName) {
        return "SELECT COUNT(*) FROM " + tableName + " WHERE " + field + " IS NOT NULL LIMIT 1";
    }
}

