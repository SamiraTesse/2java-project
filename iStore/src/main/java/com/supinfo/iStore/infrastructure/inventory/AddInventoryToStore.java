package com.supinfo.iStore.infrastructure.inventory;

public class AddInventoryToStore {

    public static String getCheckInventoryQuery() {
        return "SELECT * FROM store WHERE inventory_id = ?";
    }

    public static String getAddInventoryQuery() {
        return "UPDATE store SET inventory_id = ? WHERE name = ?";
    }

    public static String getStoreIdQuery() {
        return "UPDATE inventory SET store_id = (SELECT id FROM store WHERE name = ?) WHERE id = ?";
    }
}
