package com.supinfo.iStore.infrastructure.item;


public class AddItemsToInventory {

    public static String getCheckItemQuery() {
        return "SELECT * FROM inventory WHERE item_id = ?";
    }

    public static String getAddItemQuery() {
        return "UPDATE inventory SET item_id = ? WHERE name = ?";
    }

    public static String getCheckInventoryIdQuery() {
        return "UPDATE inventory SET store_id = (SELECT id FROM inventory WHERE name = ?) WHERE id = ?";
    }

    public static String getUpdateInventoryQuery() {
        return "UPDATE inventory SET item_id = ? WHERE ID = ?";
    }
}
