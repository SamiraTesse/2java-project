package com.supinfo.iStore.infrastructure.database;

import java.sql.*;

public class DatabaseHandler {

    public static Connection establishConnection() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/iStore";
        String username = "root";
        String password = "toto4";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            return DriverManager.getConnection(url, username, password);
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
