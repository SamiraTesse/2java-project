package com.supinfo.iStore.infrastructure.item;

public class ItemCreation {
    public static String getInsertQuery(){ return "INSERT INTO items (name, price, inventory_id) VALUES (?, ?, ?)";}

    public static String getSelectItemQuery() {
        return "SELECT COUNT(*) FROM items WHERE id = ?";
    }

    public static String getInventoryExistsQuery() {
        return "SELECT * FROM inventory WHERE ID = ?";
    }
}
