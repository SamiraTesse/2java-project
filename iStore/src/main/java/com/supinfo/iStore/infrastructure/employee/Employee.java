package com.supinfo.iStore.infrastructure.employee;

public class Employee {
    public static String getEmployeeInfos(){return "SELECT * FROM employee";}
    public static String getEmployeeById(){return "SELECT * FROM employee WHERE id=?";}

    public static String getDeleteEmployeeQuery(){return "DELETE FROM employee WHERE email=?";}

    public static String getUpdateEmployeeQuery(){return "SELECT FROM employee WHERE email=?";}


    public static String getEmailByIdQuery(){return "SELECT email FROM employee WHERE id=?";}
}
