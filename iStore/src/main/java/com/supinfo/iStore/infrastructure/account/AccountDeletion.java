package com.supinfo.iStore.infrastructure.account;

public class AccountDeletion {

    public static String getDeleteCredentialsQuery() {
        return "DELETE FROM identifiants WHERE email = ?";
    }

    public static String getDeleteInfoQueryWhitelist() {
        return "DELETE FROM whitelist WHERE email = ?";
    }

    public static String getDeleteInfoQueryWaitinglist() {
        return "DELETE FROM waitinglist WHERE id = ?";
    }

    public static String getCredentialsQuery() {
        return "SELECT * FROM identifiants WHERE email = ?";
    }

}
