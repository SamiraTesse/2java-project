package com.supinfo.iStore.infrastructure.account;


public class AccountCreation {

    public static String getSelectCredentialsQuery() {
        return "SELECT COUNT(*) FROM identifiants WHERE email = ?";
    }

    public static String getInsertCredentialsQuery() {
        return "INSERT INTO identifiants (email, password) VALUES (?, ?)";
    }

    public static String getInsertInfoQuery() {
        return "INSERT INTO waitinglist (username, name, surname, dob, email, password, role) VALUES (?, ?, ?, ?, ?, ?, ?)";
    }

    public static String getInsertQuery() {
        return "INSERT INTO whitelist (username, name, surname, DOB, email, password, role) VALUES (?, ?, ?, ?, ?, ?, ?)";
    }

    public static String getInsertIntoEmployeeQuery(){
        return "INSERT INTO employee (name,surname,dob,email, password) VALUES (?, ?, ?, ?, ?)";

    }
}
