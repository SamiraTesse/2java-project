package com.supinfo.iStore.infrastructure.store;

public class StoreDeletion {

    public static String getDeleteQuery() {
        return "DELETE FROM store WHERE name = ?";
    }
}
