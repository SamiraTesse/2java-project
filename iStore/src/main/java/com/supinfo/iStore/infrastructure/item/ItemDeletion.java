package com.supinfo.iStore.infrastructure.item;

public class ItemDeletion {

    public static String getDeleteItemByIdQuery() {
        return "DELETE FROM items WHERE id = ?";
    }
    public static String getSelectAllFromItemsQuery() {
        return "SELECT * FROM items WHERE id = ?";
    }

    public static String getDeleteItemIdFromInventoryQuery() {
        return "UPDATE inventory SET item_id = NULL WHERE item_id = ?";
    }
}
