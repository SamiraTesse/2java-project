package com.supinfo.iStore.infrastructure.account;


public class Authentication {

    public static String getSelectPasswordQuery() {
        return "SELECT password FROM identifiants WHERE email = ?";
    }


    public static String getSelectRoleQuery() {
        return "SELECT role From whitelist WHERE email = ? AND password = ?";
    }

    public static String getWaitingListInfosQuery() {
        return "SELECT COUNT(*) FROM waitinglist WHERE email=?";
    }
}
