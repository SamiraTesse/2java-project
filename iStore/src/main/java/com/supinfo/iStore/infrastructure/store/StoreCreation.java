package com.supinfo.iStore.infrastructure.store;

public class StoreCreation {
    public static String getInsertQuery() {
        return "INSERT INTO store (name) VALUES (?)";
    }

    public static String getSelectQuery() {
        return "SELECT COUNT(*) FROM store WHERE name = ?";
    }
}
