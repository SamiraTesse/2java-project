package com.supinfo.iStore.infrastructure.admin;

public class UpdateEmployee {
    public static String getUpdateEmployeeQuery(String fieldToUpdate) {
        return "UPDATE employee SET " + fieldToUpdate + " = ? WHERE email = ?";
    }
}
