package com.supinfo.iStore.ui;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MainMenuUI {
    public static void display(){
        Scanner scanner = new Scanner(System.in);

        while(true) {
            System.out.println("************************* Welcome to iStore. *************************");
            System.out.println("1. Login.");
            System.out.println("2. Signup.");
            System.out.println("3. Exit.");
            try {
                System.out.print("Enter your choice: ");
                int choice = scanner.nextInt();
                scanner.nextLine();

                switch (choice) {
                    case 1:
                        System.out.println("Selected 'Login.'");
                        LoginUI.display();
                        break;

                    case 2:
                        System.out.println("Selected 'Signup.'");
                        SignupUI.display();
                        break;

                    case 3:
                        System.out.println("Exiting...");
                        return;

                }

            } catch (InputMismatchException | SQLException e) {
                System.out.println("Invalid input. Please, enter a number.");
                scanner.nextLine();
            }
        }
//        userMenuUI.display();
    }
}
