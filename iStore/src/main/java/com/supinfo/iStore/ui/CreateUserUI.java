package com.supinfo.iStore.ui;

import com.supinfo.iStore.usecase.SignupUseCase;

import java.util.Scanner;

public class CreateUserUI {
    public static void display(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("******************* Create User *******************");
        System.out.print("Username: ");
        String username = scanner.nextLine();
        System.out.print("Name: ");
        String name = scanner.nextLine();
        System.out.print("Surname: ");
        String surname = scanner.nextLine();
        System.out.print("Date of Birth (yyyy-MM-dd): ");
        String dob = scanner.nextLine();
        System.out.print("Email: ");
        String email = scanner.nextLine();
        System.out.print("Role: ");
        String role = scanner.nextLine();
        System.out.print("Password: ");
        String password = scanner.nextLine();

        boolean signedUp = SignupUseCase.signUp(username, name, surname, dob, email, password, role);

        if (signedUp) {
            System.out.println("User successfully created!");
            EmployeeMenuUI.display();
        }
        else {
            System.out.println("Unable to create user. User already exists or invalid input.");
            System.out.print("Do you want to try again? (yes/no): ");
            String choice = scanner.nextLine();
            if (choice.equalsIgnoreCase("yes")) {
                display();
            } else {
                EmployeeMenuUI.display();
            }
        }
    }
}
