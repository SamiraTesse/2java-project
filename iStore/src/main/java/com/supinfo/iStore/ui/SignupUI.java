package com.supinfo.iStore.ui;

import com.supinfo.iStore.usecase.SignupUseCase;

import java.sql.SQLException;
import java.util.Scanner;

public class SignupUI {

    public static void display() throws SQLException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to iStore!");

        System.out.println("****************************************************** Signup ******************************************************");
        System.out.print("Username: ");
        String username = scanner.nextLine();
        System.out.print("Name: ");
        String name = scanner.nextLine();
        System.out.print("Surname: ");
        String surname = scanner.nextLine();
        System.out.print("Date of Birth (yyyy-MM-dd): ");
        String dob = scanner.nextLine();
        System.out.print("Email: ");
        String email = scanner.nextLine();
        System.out.print("Role: ");
        String role = scanner.nextLine();
        System.out.print("Password: ");
        String password = scanner.nextLine();

        boolean signedUp = SignupUseCase.signUp(username, name, surname, dob, email, password, role);

        if (signedUp) {
            System.out.println("Signup successful!");
            System.out.println("Do you want to login now? (yes/no)");
            String choice = scanner.nextLine();
            if (choice.equalsIgnoreCase("yes")) {
                LoginUI.display();
            } else {
                System.out.println("Thank you for signing up. Goodbye!");
            }
        } else {
            System.out.println("Signup failed. User already exists or invalid input.");
            System.out.println("Do you want to try again? (yes/no)");
            String choice = scanner.nextLine();
            if (choice.equalsIgnoreCase("yes")) {
                display();
            } else {
                System.out.println("Thank you for visiting. Goodbye!");
            }
        }

    }
}
