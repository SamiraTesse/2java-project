package com.supinfo.iStore.ui;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.supinfo.iStore.usecase.admins.item.ItemDisplayUseCase;
import com.supinfo.iStore.usecase.employees.DisplayStoreUseCase;
import com.supinfo.iStore.usecase.employees.EmployeeDisplayInfoUseCase;
import com.supinfo.iStore.usecase.UserDeleteProfileUseCase;
import com.supinfo.iStore.usecase.UserUpdateProfileUseCase;

public class EmployeeMenuUI {

    public static void display() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("User Menu:");
            System.out.println("1. Create a new user.");
            System.out.println("2. See Other Users.");
            System.out.println("3. See stores.");
            System.out.println("4. See inventories.");
            System.out.println("5. Update Profile.");
            System.out.println("6. Delete Account.");
            System.out.println("7. Logout.");

            try {
                System.out.print("Enter your choice: ");
                int choice = scanner.nextInt();
                scanner.nextLine();

                switch (choice) {
                    case 1:
                        System.out.println("Selected 'Create a new user.'");
                        CreateUserUI.display();
                        break;
                    case 2:
                        System.out.println("Selected 'See Other Users.'");
                        System.out.println("Displaying other users...");
                        EmployeeDisplayInfoUseCase.display();
                        break;
                    case 4:
                        System.out.println("Selected 'See inventories.'");
                        ItemDisplayUseCase.display();
                        break;
                    case 3:
                        System.out.println("Selected 'See stores.'");
                        DisplayStoreUseCase.display();
                        break;
                    case 5:
                        System.out.println("Selected 'Update Profile.'");
                        UserUpdateProfileUseCase.updateProfileUI(scanner);
                        break;
                    case 6:
                        System.out.println("Selected 'Delete Account.'");
                        deleteAccount(scanner);
                        break;
                    case 7:
                        System.out.println("Logging out...");
                        return;
                    default:
                        System.out.println("Invalid choice. Please enter a valid option.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please, enter a number.");
                scanner.nextLine();
            }
        }
    }

    private static void deleteAccount(Scanner scanner) {
        System.out.print("Are you sure you want to delete your account? (yes/no): ");
        String confirmation = scanner.nextLine();
        if (confirmation.equalsIgnoreCase("yes")) {
            System.out.print("Enter your email: ");
            String email = scanner.nextLine();
            System.out.print("Enter your password: ");
            String password = scanner.nextLine();

            if (UserDeleteProfileUseCase.deleteProfile(email, password)) {
                System.out.println("Account successfully deleted.");
                MainMenuUI.display();
                return;
            } else {
                System.out.println("Failed to delete account.");
            }
        } else {
            System.out.println("Account deletion cancelled.");
        }
    }

}
