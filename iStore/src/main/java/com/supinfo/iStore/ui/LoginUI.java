package com.supinfo.iStore.ui;

import com.supinfo.iStore.entity.Account;
import com.supinfo.iStore.usecase.LoginUseCase;

import java.sql.SQLException;
import java.util.Scanner;

public class LoginUI {

    public static void display() throws SQLException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to iStore!");

        System.out.println("****************************************************** Login ******************************************************");
        System.out.print("Email: ");
        String loginEmail = scanner.nextLine();
        System.out.print("Password: ");
        String loginPassword = scanner.nextLine();

        boolean loggedIn = LoginUseCase.login(loginEmail, loginPassword);

        if (loggedIn) {
            System.out.println("Welcome!");
            if (Account.isAdmin(loginEmail))
                AdminMenuUI.display();
            else
                EmployeeMenuUI.display();
        }

        else {
            System.out.println("Login failed. Please check your credentials.");
        }

    }
}
