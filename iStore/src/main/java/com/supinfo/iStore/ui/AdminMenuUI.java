package com.supinfo.iStore.ui;

import com.supinfo.iStore.entity.AdminAccount;
import com.supinfo.iStore.usecase.*;
import com.supinfo.iStore.usecase.admins.*;
import com.supinfo.iStore.usecase.admins.employeeManagement.DeleteEmployeeAccountUseCase;
import com.supinfo.iStore.usecase.admins.inventory.InventoryDisplayInfosUseCase;
import com.supinfo.iStore.usecase.admins.item.CreateItemUseCase;
import com.supinfo.iStore.usecase.admins.item.DeleteItemUseCase;
import com.supinfo.iStore.usecase.admins.item.ItemDisplayUseCase;
import com.supinfo.iStore.usecase.admins.store.*;
import com.supinfo.iStore.usecase.employees.EmployeeUseCase;

import java.sql.*;
import java.util.Scanner;

public class AdminMenuUI {
    public static void display() throws SQLException {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            makeSpace();
            System.out.println("Menu:");
            System.out.println("1. See Waiting list");
            System.out.println("2. See Whitelist");
            System.out.println("3. Update your profile");
            System.out.println("4. Manage employees accounts");
            System.out.println("5. Manage stores");
            System.out.println("6. Manage inventories");
            System.out.println("7. Logout");

            System.out.print("Enter your choice: ");
            int choice = scanner.nextInt();
            resetScannerToString(scanner);
            makeSpace();

            switch (choice) {
                case 1:
                    System.out.println("Selected 'See Waiting list'");
                    WaitinglistUseCase.display();
                    System.out.println("Do you wanna whitelist an account(yes/no)?");
                    String whitelistingChoice = scanner.nextLine();
                    if (whitelistingChoice.equalsIgnoreCase("yes")) {
                        System.out.print("Select the ID to whitelist: ");
                        int selectedID = scanner.nextInt();
                        WhitelistingUseCase.addToWhiteList(selectedID);

                    }
                    break;
                case 2:
                    System.out.println("Selected 'See Whitelist'");
                    WhitelistingUseCase.display();
                    break;
                case 3:
                    System.out.println("Selected 'Update your profile'");
                    UserUpdateProfileUseCase.updateProfileUI(scanner);
                    break;
                case 4:
                    System.out.println("Selected 'Manage employees accounts'");
                    EmployeeUseCase.display();

                    makeSpace();
                    System.out.println("Options:");
                    System.out.println("1. Update account");
                    System.out.println("2. Delete account");
                    System.out.println("3. Back to menu");


                    System.out.print("Enter your choice: ");
                    int employeeChoice = scanner.nextInt();
                    makeSpace();
                    switch (employeeChoice) {
                        case 1:
                            AdminAccount.updateEmployeeProfile(scanner);
                            break;
                        case 2:
                            System.out.print("Select ID to delete: ");
                            int deletionId = scanner.nextInt();
                            resetScannerToString(scanner);
                            System.out.println("Are you sure you want to delete the account?(yes/no)");
                            String deletionConfirmation = scanner.nextLine();
                            if (deletionConfirmation.equalsIgnoreCase("yes")) {
                                DeleteEmployeeAccountUseCase.deleteEmployeeProfile(deletionId);
                            }
                            break;
                        case 3:
                            break;
                        default:
                            System.out.println("Invalid choice. Please enter a valid option.");
                    } break;

                case 5:
                    System.out.println("Selected 'Manage stores'");
                    StoreDisplayInfosUseCase.display();

                    System.out.println("Options:");
                    System.out.println("1. Create a new store.");
                    System.out.println("2. Delete store");
                    System.out.println("3. Add inventory to store");
                    System.out.println("4. Add employee to store");
                    System.out.println("5. Display Access List");
                    System.out.println("6. Back to menu");

                    System.out.print("Enter your choice: ");
                    int storeChoice = scanner.nextInt();
                    resetScannerToString(scanner);
                    makeSpace();
                    switch (storeChoice){
                        case 1:
                            System.out.println("Selected 'Create store'.");

                            System.out.print("Enter the store name: ");
                            String storeNameToCreate = scanner.nextLine();
                            CreateStoreUseCase.createStore(storeNameToCreate);
                            break;
                        case 2:
                            System.out.println("Selected 'Delete store'.");

                            System.out.print("Enter the store name: ");
                            String storeNameToDelete = scanner.nextLine();
                            DeleteStoreUseCase.deleteStore(storeNameToDelete);
                            break;
                        case 3:
                            System.out.println("Selected 'Add inventory'.");
                            InventoryDisplayInfosUseCase.display();
                            StoreAddInventoryUseCase.addInventoryToStore(scanner);
                            break;
                        case 4:
                            System.out.println("Selected 'Add employee'.");
                            EmployeeUseCase.display();
                            StoreAddEmployeeUseCase.addEmployeeToStore(scanner);
                            break;
                        case 5:
                            System.out.println("Selected 'Display access list'.");
                            System.out.print("Enter the name of the store: ");
                            String storeNameToDisplay = scanner.nextLine();
                            StoreDisplayAccessList.display(storeNameToDisplay);
                            break;
                        case 6:
                            break;
                        default:
                            System.out.println("Invalid choice. Please enter a valid option.");
                    }
                    break;

                case 6:
                    System.out.println("Selected 'Manage inventories'");
                    ItemDisplayUseCase.display();

                    System.out.println("Options:");
                    System.out.println("1. Create a new item.");
                    System.out.println("2. Delete an item");

                    System.out.print("Enter your choice: ");
                    int itemChoice = scanner.nextInt();
                    resetScannerToString(scanner);

                    switch (itemChoice) {
                        case 1:
                            CreateItemUseCase.createItem(scanner);
                            break;
                        case 2:
                            System.out.println("Are you sure you want to delete the item?(yes/no)");
                            String deletionConfirmation = scanner.nextLine();
                            if (deletionConfirmation.equalsIgnoreCase("yes")) {
                                DeleteItemUseCase.deleteItem(scanner);
                            }
                            break;
                        case 3:
                            break;
                        default:
                            System.out.println("Invalid choice. Please enter a valid option.");
                    } break;

                case 7:
                    System.out.println("Logging out...");
                    return;
                default:
                    System.out.println("Invalid choice. Please enter a valid option.");
            }
        }

    }

    private static void makeSpace() {
        System.out.print("\n");
    }

    private static void resetScannerToString(Scanner scanner) {
        scanner.nextLine();
    }

}