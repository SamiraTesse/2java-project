package com.supinfo.iStore.usecase.admins.store;

import com.supinfo.iStore.entity.Store;
import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.store.StoreCreation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CreateStoreUseCase {

    public static void createStore(String storeName) {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            if (Store.isStoreExists(connection, storeName)) {
                System.out.println("Store with the same name already exists.");
                return;
            }

            String insertQuery = StoreCreation.getInsertQuery();
            try (PreparedStatement preparedStatement = connection.prepareStatement(insertQuery)) {
                preparedStatement.setString(1, storeName);
                int rowsAffected = preparedStatement.executeUpdate();
                if (rowsAffected > 0) {
                    System.out.println("Store created successfully.");
                } else {
                    System.out.println("Failed to create store.");
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
    }
}
