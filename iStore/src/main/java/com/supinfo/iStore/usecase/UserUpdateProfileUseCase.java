package com.supinfo.iStore.usecase;

import com.supinfo.iStore.entity.Account;

import java.util.Scanner;

public class UserUpdateProfileUseCase {

    public static void updateProfileUI(Scanner scanner) {
        System.out.println("Update Profile:");
        System.out.print("Enter your email: ");
        String email = scanner.nextLine();
        System.out.print("Enter the field you want to update (username/name/surname/dob/email/password/role): ");
        String fieldToUpdate = scanner.nextLine();
        System.out.print("Enter the new value: ");
        String newValue = scanner.nextLine();
        Account.updateProfile(email, fieldToUpdate, newValue);
    }

}
