package com.supinfo.iStore.usecase.admins.inventory;

import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.inventory.InventoryDisplay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class InventoryDisplayInfosUseCase {
    public static void display() {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            String query = InventoryDisplay.getSelectQuery();
            assert connection != null;
            try (PreparedStatement preparedStatement = connection.prepareStatement(query);
                 ResultSet resultSet = preparedStatement.executeQuery()) {
                    System.out.println("List of Inventories:");
                    System.out.println("| ID  | Inventory Name    | Store ID |");
                    while (resultSet.next()) {
                        int id = resultSet.getInt("id");
                        String inventoryName = resultSet.getString("name");
                        int storeId = resultSet.getInt("store_id");
                        System.out.printf("| %-3d | %-17s | %-12d |\n", id, inventoryName, storeId);
                    }
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
    }
}
