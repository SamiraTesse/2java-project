package com.supinfo.iStore.usecase;

import com.supinfo.iStore.entity.Account;
import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.supinfo.iStore.infrastructure.account.AccountCreation.*;

public class SignupUseCase {

    public static boolean signUp(String username, String name, String surname, String dob, String email, String plainPassword, String role) {
        try {
            if (!isValidEmail(email)) {
                System.out.println("Invalid email format. Please enter a valid email address.");
                return false;
            }

            if (!isValidDOB(dob)) {
                System.out.println("Invalid date of birth format. Please enter a valid date in format 'yyyy-MM-dd'.");
                return false;
            }

            try (Connection connection = DatabaseHandler.establishConnection()) {
                if (connection == null) {
                    System.err.println("Error 404. Something went wrong!");
                    return false;
                }

                if (Account.userExists(connection, email)) {
                    System.out.println("User already exists.");
                    return false;
                }

                String hashedPassword = BCrypt.hashpw(plainPassword, BCrypt.gensalt());
                String insertInfoQuery = getInsertInfoQuery();
                try (PreparedStatement infoStatement = connection.prepareStatement(insertInfoQuery)) {
                    infoStatement.setString(1, username);
                    infoStatement.setString(2, name);
                    infoStatement.setString(3, surname);
                    infoStatement.setDate(4, java.sql.Date.valueOf(dob));
                    infoStatement.setString(5, email);
                    infoStatement.setString(6, hashedPassword);
                    infoStatement.setString(7, role);

                    int infoRowsAffected = infoStatement.executeUpdate();

                    if (infoRowsAffected > 0) {
                        System.out.println("User information saved successfully!");
                        return true;
                    } else {
                        System.out.println("Failed to save user information.");
                    }
                }
//                registerCredentials(email, connection, hashedPassword);
            } catch (SQLException ex) {
                System.out.println("An error occurred while processing your request. Please try again later.");
                return false;
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Date is incorrect. Please check your input data and try again.");
            return false;
        }

        return false;
    }

    public static void insertIntoEmployee(Connection connection, String name, String surname, Date dob, String email, String hashedPassword) throws SQLException {
        String insertInfosQuery = getInsertIntoEmployeeQuery();
        try (PreparedStatement infosStatement = connection.prepareStatement(insertInfosQuery)) {
            infosStatement.setString(1, name);
            infosStatement.setString(2, surname);
            infosStatement.setDate(3, dob);
            infosStatement.setString(4, email);
            infosStatement.setString(5, hashedPassword);

            int infosRowsAffected = infosStatement.executeUpdate();

            if (infosRowsAffected > 0) {
                System.out.println("Employee infos saved successfully!");

            } else {
                System.out.println("Failed to save Employee infos.");
            }
        }
    }

    private static boolean isValidEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private static boolean isValidDOB(String dob) {
        String dobRegex = "\\d{4}-\\d{2}-\\d{2}";
        return dob.matches(dobRegex);
    }

}
