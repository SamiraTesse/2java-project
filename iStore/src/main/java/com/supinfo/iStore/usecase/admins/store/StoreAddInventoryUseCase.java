package com.supinfo.iStore.usecase.admins.store;

import com.supinfo.iStore.entity.Store;
import com.supinfo.iStore.infrastructure.inventory.AddInventoryToStore;
import com.supinfo.iStore.infrastructure.database.DatabaseHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class StoreAddInventoryUseCase {

    public static void addInventoryToStore(Scanner scanner) {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            System.out.print("Enter the ID of the inventory: ");
            int inventoryId = scanner.nextInt();
            scanner.nextLine(); // Consume newline character

            System.out.print("Enter the name of the store: ");
            String storeName = scanner.nextLine();

            if (!Store.isStoreExists(connection, storeName)) {
                System.out.println("Store does not exist.");
                return;
            }

            // Check if the inventory_id is already added to a store
            String checkInventoryQuery = AddInventoryToStore.getCheckInventoryQuery();
            assert connection != null;
            try (PreparedStatement checkInventoryStatement = connection.prepareStatement(checkInventoryQuery)) {
                checkInventoryStatement.setInt(1, inventoryId);
                try (ResultSet resultSet = checkInventoryStatement.executeQuery()) {
                    if (resultSet.next()) {
                        System.out.println("Inventory ID already added to a store.");
                        return;
                    }
                }
            }

            // Add inventory to store
            String addInventoryQuery = AddInventoryToStore.getAddInventoryQuery();
            try (PreparedStatement addInventoryStatement = connection.prepareStatement(addInventoryQuery)) {
                addInventoryStatement.setInt(1, inventoryId);
                addInventoryStatement.setString(2, storeName);
                int rowsAffected = addInventoryStatement.executeUpdate();
                if (rowsAffected > 0) {
                    System.out.println("Inventory added to store successfully.");
                    // Set store_id in the inventory table
                    String setStoreIdQuery = AddInventoryToStore.getStoreIdQuery();
                    try (PreparedStatement setStoreIdStatement = connection.prepareStatement(setStoreIdQuery)) {
                        setStoreIdStatement.setString(1, storeName);
                        setStoreIdStatement.setInt(2, inventoryId);
                        int storeIdRowsAffected = setStoreIdStatement.executeUpdate();
                        if (storeIdRowsAffected > 0) {
                            System.out.println("Store ID set in inventory table.");
                        } else {
                            System.out.println("Failed to set store ID in inventory table.");
                        }
                    }
                } else {
                    System.out.println("Failed to add inventory to store.");
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
    }

}
