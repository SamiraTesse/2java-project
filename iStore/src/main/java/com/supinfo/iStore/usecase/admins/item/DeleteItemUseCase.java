package com.supinfo.iStore.usecase.admins.item;

import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.item.ItemDeletion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class DeleteItemUseCase {

    public static void deleteItem(Scanner scanner) {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            System.out.print("Enter item ID to delete: ");
            int itemId = scanner.nextInt();

            if (itemExists(connection, itemId)) {
                assert connection != null;
                deleteItemFromInventory(connection, itemId);
                deleteItemFromItems(connection, itemId);
                System.out.println("Item deleted successfully.");
            } else {
                System.out.println("Item with ID " + itemId + " does not exist.");
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
    }

    private static boolean itemExists(Connection connection, int itemId) throws SQLException {
        String query = ItemDeletion.getSelectAllFromItemsQuery();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, itemId);
            return preparedStatement.executeQuery().next();
        }
    }

    private static void deleteItemFromInventory(Connection connection, int itemId) throws SQLException {
        String query = ItemDeletion.getDeleteItemIdFromInventoryQuery();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, itemId);
            preparedStatement.executeUpdate();
        }
    }

    private static void deleteItemFromItems(Connection connection, int itemId) throws SQLException {
        String query = ItemDeletion.getDeleteItemByIdQuery();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, itemId);
            preparedStatement.executeUpdate();
        }
    }
}
