package com.supinfo.iStore.usecase.admins.item;

import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.item.ItemsDisplay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ItemDisplayUseCase {
    public static void display() {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            String query = ItemsDisplay.getSelectQuery();
            assert connection != null;
            try (PreparedStatement preparedStatement = connection.prepareStatement(query);
                 ResultSet resultSet = preparedStatement.executeQuery()) {
                System.out.println("List of Items:");
                System.out.println("| ID  |  Name         |  Price  | Inventory ID |");
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String itemName = resultSet.getString("name");
                    int itemPrice = resultSet.getInt("price");
                    int inventoryId = resultSet.getInt("inventory_id");
                    System.out.printf("| %-3d | %-17s | %-12d | %-12d |\n", id, itemName, itemPrice, inventoryId);
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
    }
}
