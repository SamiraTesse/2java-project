package com.supinfo.iStore.usecase;

public class SuccessUseCaseResponse implements UseCaseResponse{
    @Override
    public boolean hasSucceed() {
        return true;
    }
}
