package com.supinfo.iStore.usecase.admins;

import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.admin.WaitingList;

import java.sql.*;

public class WaitinglistUseCase {
    public static void display() throws SQLException {
        try (Connection connection = DatabaseHandler.establishConnection()) {

            String request = WaitingList.getWaitingListInfos();
            try {
                assert connection != null;
                try (Statement stmt = connection.createStatement();
                         ResultSet res = stmt.executeQuery(request)) {

                    System.out.println("Waiting List:");
                    System.out.println("| ID  | Name       | Surname         | Username        | Email                      | Role           |");

                    while (res.next()) {
                        int id = res.getInt("ID");
                        String name = res.getString("Name");
                        String surname = res.getString("Surname");
                        String username = res.getString("Username");
                        String email = res.getString("Email");
                        String role = res.getString("Role");
                        System.out.printf("| %-3d | %-10s | %-15s | %-15s | %-25s | %-15s |\n", id, name, surname, username, email, role);
                    }

                }
            } catch (SQLException ex) {
                System.out.println("SQLException: " + ex.getMessage() + '.');

            }
        }
    }



}
