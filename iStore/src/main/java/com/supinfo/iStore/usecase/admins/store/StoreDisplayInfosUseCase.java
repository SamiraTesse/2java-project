package com.supinfo.iStore.usecase.admins.store;

import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.store.StoreDisplay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class StoreDisplayInfosUseCase {
    public static void display() {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            String query = StoreDisplay.getSelectQuery();
            assert connection != null;
            try (PreparedStatement preparedStatement = connection.prepareStatement(query);
                 ResultSet resultSet = preparedStatement.executeQuery()) {
                System.out.println("List of Stores:");
                System.out.println("| ID  | Store Name    | Inventory ID | Employee ID |");
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String storeName = resultSet.getString("name");
                    int inventoryId = resultSet.getInt("inventory_id");
                    int employeeId = resultSet.getInt("employee_id");
                    System.out.printf("| %-3d | %-13s | %-12d | %-11d |\n", id, storeName, inventoryId, employeeId);
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
    }
}
