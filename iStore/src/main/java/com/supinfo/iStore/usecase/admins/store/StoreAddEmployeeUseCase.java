package com.supinfo.iStore.usecase.admins.store;

import com.supinfo.iStore.entity.Store;
import com.supinfo.iStore.infrastructure.employee.AddEmployeeToStore;
import com.supinfo.iStore.infrastructure.database.DatabaseHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class StoreAddEmployeeUseCase {

    public static void addEmployeeToStore(Scanner scanner) {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            System.out.print("Enter the ID of the employee: ");
            int employeeId = scanner.nextInt();
            scanner.nextLine();

            System.out.print("Enter the name of the store: ");
            String storeName = scanner.nextLine();

            // Check if the store exists
            if (!Store.isStoreExists(connection, storeName)) {
                System.out.println("Store does not exist.");
                return;
            }

            int storeId = Store.getStoreIdByName(connection, storeName);

            String insertEmployeeQuery = AddEmployeeToStore.getAddEmployeeQuery();
            assert connection != null;
            try (PreparedStatement insertEmployeeStatement = connection.prepareStatement(insertEmployeeQuery)) {
                insertEmployeeStatement.setInt(1, employeeId);
                insertEmployeeStatement.setString(2, storeName); // Set store_id
                int rowsAffected = insertEmployeeStatement.executeUpdate();
                if (rowsAffected > 0) {
                    System.out.println("Employee added to store successfully.");
                    String updateEmployeeQuery = AddEmployeeToStore.getUpdateEmployeeQuery();
                    try (PreparedStatement updateEmployeeStatement = connection.prepareStatement(updateEmployeeQuery)) {
                        updateEmployeeStatement.setInt(1, storeId);
                        updateEmployeeStatement.setInt(2, employeeId);
                        updateEmployeeStatement.executeUpdate();
                    }
                } else {
                    System.out.println("Failed to add employee to store.");
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
    }

}
