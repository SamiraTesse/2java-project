package com.supinfo.iStore.usecase;

public interface UseCaseResponse {
    boolean hasSucceed();

    static UseCaseResponse failedUseCaseResponse(){
        return new FailedUseCaseResponse();
    }

    static UseCaseResponse successUseCaseResponse(){
        return new SuccessUseCaseResponse();
    }
}
