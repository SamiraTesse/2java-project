package com.supinfo.iStore.usecase;

public class LoginFailedException extends Exception{

    public LoginFailedException(String message) {
        super(message);
    }
}
