package com.supinfo.iStore.usecase.employees;

import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.employee.Employee;

import java.sql.*;

public class EmployeeUseCase {
    public static void display() throws SQLException {
        try (Connection connection = DatabaseHandler.establishConnection()) {

            String request = Employee.getEmployeeInfos();
            try {
                assert connection != null;
                try (PreparedStatement preparedStatement = connection.prepareStatement(request)) {
                    try (ResultSet resultSet = preparedStatement.executeQuery()) {

                        System.out.println("Employee List:");
                        System.out.println("| ID  | Name       | Surname         | Email                     | DOB             | Store_id   | ");

                        while (resultSet.next()) {
                            int id = resultSet.getInt("ID");
                            String name = resultSet.getString("Name");
                            String surname = resultSet.getString("Surname");
                            String email = resultSet.getString("Email");
                            Date DOB = resultSet.getDate("DOB");
                            int storeId = resultSet.getInt("store_id");
                            System.out.printf("| %-3d | %-10s | %-15s | %-25s | %-15s | %-10d |\n", id, name, surname, email, DOB, storeId);
                        }

                    }
                }
            } catch (SQLException ex) {
                System.out.println("SQLException: " + ex.getMessage() + '.');

            }
        }

    }
    }