package com.supinfo.iStore.usecase.admins;

import com.supinfo.iStore.infrastructure.account.AccountCreation;
import com.supinfo.iStore.infrastructure.account.AccountDeletion;
import com.supinfo.iStore.infrastructure.admin.Whitelist;
import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.usecase.SignupUseCase;

import java.sql.*;
import java.util.InputMismatchException;

public class WhitelistingUseCase {

    public static void addToWhiteList(int id) {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            String selectQuery = Whitelist.getSelectQuery();
            assert connection != null;
            try (PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
                selectStatement.setInt(1, id);
                try (ResultSet resultSet = selectStatement.executeQuery()) {
                    if (resultSet.next()) {
                        String name = resultSet.getString("name");
                        String surname = resultSet.getString("surname");
                        String username = resultSet.getString("username");
                        String email = resultSet.getString("email");
                        String password = resultSet.getString("password");
                        Date DOB = resultSet.getDate("DOB");
                        String role = resultSet.getString("role");

                        // Add data to whitelist table
                        String insertQuery = AccountCreation.getInsertQuery();
                        try (PreparedStatement insertStatement = connection.prepareStatement(insertQuery)) {
                            insertStatement.setString(1, username);
                            insertStatement.setString(2, name);
                            insertStatement.setString(3, surname);
                            insertStatement.setDate(4, DOB);
                            insertStatement.setString(5, email);
                            insertStatement.setString(6, password);
                            insertStatement.setString(7, role);

                            int rowsAffected = insertStatement.executeUpdate();
                            if (rowsAffected > 0) {
                                if (role.equalsIgnoreCase("employee")){
                                    SignupUseCase.insertIntoEmployee(connection, name, username, DOB, email, password);
                                }
                                System.out.println("Data added to whitelist successfully.");

                                String insertCredentialsQuery = AccountCreation.getInsertCredentialsQuery();
                                try (PreparedStatement credentialsStatement = connection.prepareStatement(insertCredentialsQuery)) {
                                    credentialsStatement.setString(1, email);
                                    credentialsStatement.setString(2, password);
                                    credentialsStatement.executeUpdate();

                                    String deleteQuery = AccountDeletion.getDeleteInfoQueryWaitinglist();
                                    try (PreparedStatement deleteStatement = connection.prepareStatement(deleteQuery)) {
                                        deleteStatement.setInt(1, id);
                                        deleteStatement.executeUpdate();
                                        System.out.println("Row deleted from waitinglist successfully.");
                                    }
                                }
                            }
                        }
                    } else {
                        System.out.println("No data found in waitinglist with id: " + id);
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please enter a number.");
        }
    }


    public static void display() {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            if (connection == null) {
                System.err.println("Failed to establish database connection.");
                return;
            }

            String query = Whitelist.getWhiteListInfos();
            try (Statement statement = connection.createStatement();
                 ResultSet resultSet = statement.executeQuery(query)) {

                System.out.println("White List:");
                System.out.println("| ID  | Name       | Surname         | Username        | Role                      | Email           |");

                while (resultSet.next()) {
                    int id = resultSet.getInt("ID");
                    String name = resultSet.getString("Name");
                    String surname = resultSet.getString("Surname");
                    String username = resultSet.getString("Username");
                    String email = resultSet.getString("Email");
                    String role = resultSet.getString("Role");
                    System.out.printf("| %-3d | %-10s | %-15s | %-15s | %-25s | %-15s |\n", id, name, surname, username, role, email);
                }
            } catch (SQLException ex) {
                System.err.println("Failed to execute SQL query: " + ex.getMessage());
            }
        } catch (SQLException ex) {
            System.err.println("Failed to establish database connection: " + ex.getMessage());
        }
    }


}
