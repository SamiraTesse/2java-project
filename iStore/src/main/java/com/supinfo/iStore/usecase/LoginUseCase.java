package com.supinfo.iStore.usecase;

import com.supinfo.iStore.entity.Account;
import org.mindrot.jbcrypt.BCrypt;
import com.supinfo.iStore.infrastructure.database.DatabaseHandler;

import java.sql.Connection;
import java.sql.SQLException;


public class LoginUseCase {

    public static boolean login(String email, String plainPassword) {

        try (Connection connection = DatabaseHandler.establishConnection()) {
            String hashedPassword = Account.getHashedPassword(connection, email);

            if (Account.isWaiting(email)) {
                System.out.println("Your account is being validated!");
                return false;
            }
            else{
                if (hashedPassword == null) {
                    System.out.println("Email or password incorrect.");
                    return false;
                }


                else if (BCrypt.checkpw(plainPassword, hashedPassword)) {
                    System.out.println("Login successful!");
                    return true;
                } else {
                    System.out.println("Email or password incorrect.");
                    return false;
                }
            }


        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage() + '.');
            return false;
        }
    }

}
