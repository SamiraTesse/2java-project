package com.supinfo.iStore.usecase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.supinfo.iStore.infrastructure.account.AccountDeletion;
import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import org.mindrot.jbcrypt.BCrypt;

public class UserDeleteProfileUseCase {

    public static boolean deleteProfile(String email, String password) {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            if (connection == null) {
                System.err.println("Error 404. Something went wrong!");
                return false;
            }
            String checkCredentialsQuery = AccountDeletion.getCredentialsQuery();
            try (PreparedStatement checkStatement = connection.prepareStatement(checkCredentialsQuery)) {
                checkStatement.setString(1, email);

                try (ResultSet resultSet = checkStatement.executeQuery()) {
                    if (resultSet.next()) {
                        String hashedPassword = resultSet.getString("password");
                        if (BCrypt.checkpw(password, hashedPassword)) {
                            String deleteInfoQuery = AccountDeletion.getDeleteInfoQueryWhitelist();
                            try (PreparedStatement infoStatement = connection.prepareStatement(deleteInfoQuery)) {
                                infoStatement.setString(1, email);
                                infoStatement.executeUpdate();
                            }

                            String deleteCredentialsQuery = AccountDeletion.getDeleteCredentialsQuery();
                            try (PreparedStatement credentialsStatement = connection.prepareStatement(deleteCredentialsQuery)) {
                                credentialsStatement.setString(1, email);
                                credentialsStatement.executeUpdate();
                            }
                            return true;
                        }
                    }
                }
            }
            return false;
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage() + '.');
            return false;
        }
    }

}
