package com.supinfo.iStore.usecase.employees;

import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.store.StoreAccessList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DisplayStoreUseCase {
    public static void display() {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            if (connection != null) {
                String query = StoreAccessList.getStoreIdQuery();
                try (PreparedStatement statement = connection.prepareStatement(query);
                     ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        int storeId = resultSet.getInt("store_id");
                        String storeInfoQuery = StoreAccessList.getStoreInfoQuery();
                        try (PreparedStatement storeInfoStatement = connection.prepareStatement(storeInfoQuery)) {
                            storeInfoStatement.setInt(1, storeId);
                            try (ResultSet storeResultSet = storeInfoStatement.executeQuery()) {
                                while (storeResultSet.next()) {
                                    int id = storeResultSet.getInt("id");
                                    String name = storeResultSet.getString("name");
                                    System.out.printf("Store ID: %d, Name: %s%n", id, name);
                                }
                            }
                        }
                    }
                }
            } else {
                System.out.println("Failed to establish database connection.");
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
    }

}
