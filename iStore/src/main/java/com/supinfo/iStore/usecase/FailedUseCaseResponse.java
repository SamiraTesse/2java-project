package com.supinfo.iStore.usecase;

public class FailedUseCaseResponse implements UseCaseResponse{
    @Override
    public boolean hasSucceed() {
        return false;
    }
}
