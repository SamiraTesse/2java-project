package com.supinfo.iStore.usecase.admins.employeeManagement;

import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.admin.UpdateEmployee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UpdateEmployeeAccountUseCase {
    public static void updateEmployeeProfile(String email, String fieldToUpdate, String newValue){
        try (Connection connection = DatabaseHandler.establishConnection()) {
            if (connection == null) {
                System.err.println("Error 404. Something went wrong!");
                return;
            }
            String updateQuery = UpdateEmployee.getUpdateEmployeeQuery(fieldToUpdate);
            try (PreparedStatement preparedStatement = connection.prepareStatement(updateQuery)) {
                preparedStatement.setString(1, newValue);
                preparedStatement.setString(2, email);

                int rowsAffected = preparedStatement.executeUpdate();

                if (rowsAffected > 0) {
                    System.out.println("Profile updated successfully!");
                } else {
                    System.out.println("Failed to update profile.");
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage() + '.');
        }
    }
}
