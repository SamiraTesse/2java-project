package com.supinfo.iStore.usecase.admins.store;

import com.supinfo.iStore.entity.Store;
import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.store.StoreDeletion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DeleteStoreUseCase {
    public static void deleteStore(String storeName) {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            if (!Store.isStoreExists(connection, storeName)) {
                System.out.println("Store does not exist.");
                return;
            }

            String deleteQuery = StoreDeletion.getDeleteQuery();
            try (PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery)) {
                preparedStatement.setString(1, storeName);
                int rowsAffected = preparedStatement.executeUpdate();
                if (rowsAffected > 0) {
                    System.out.println("Store deleted successfully.");
                } else {
                    System.out.println("Failed to delete store.");
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
    }


}
