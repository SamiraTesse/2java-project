package com.supinfo.iStore.usecase.admins.item;

import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.item.AddItemsToInventory;
import com.supinfo.iStore.infrastructure.item.ItemCreation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class CreateItemUseCase {
    public static void createItem(Scanner scanner) {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            System.out.print("Enter item name: ");
            String name = scanner.nextLine();
            System.out.print("Enter item price: ");
            int price = scanner.nextInt();
            System.out.print("Enter inventory id: ");
            int inventory_id = scanner.nextInt();

            if (!inventoryExists(connection, inventory_id)) {
                System.out.println("The inventory with the selected id does not exist.");
                return;
            }

            String insertQuery = ItemCreation.getInsertQuery();

            assert connection != null;
            try (PreparedStatement preparedStatement = connection.prepareStatement(insertQuery, PreparedStatement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, name);
                preparedStatement.setInt(2, price);
                preparedStatement.setInt(3, inventory_id);
                int rowsAffected = preparedStatement.executeUpdate();
                if (rowsAffected > 0) {
                    ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                    if (generatedKeys.next()) {
                        int itemId = generatedKeys.getInt(1);
                        updateInventoryWithItemId(connection, itemId, inventory_id);
                    }
                    System.out.println("Item created successfully.");
                } else {
                    System.out.println("Failed to create item.");
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
    }

    private static boolean inventoryExists(Connection connection, int inventory_id) throws SQLException {
        String query = ItemCreation.getInventoryExistsQuery();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, inventory_id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return resultSet.next();
            }
        }
    }

    private static void updateInventoryWithItemId(Connection connection, int itemId, int inventory_id) throws SQLException {
        String updateQuery = AddItemsToInventory.getUpdateInventoryQuery();
        try (PreparedStatement preparedStatement = connection.prepareStatement(updateQuery)) {
            preparedStatement.setInt(1, itemId);
            preparedStatement.setInt(2, inventory_id);
            preparedStatement.executeUpdate();
        }
    }

}
