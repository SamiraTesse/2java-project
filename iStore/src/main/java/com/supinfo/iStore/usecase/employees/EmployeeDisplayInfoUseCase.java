package com.supinfo.iStore.usecase.employees;

import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.employee.Employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeDisplayInfoUseCase {

    public static void display() {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            if (connection == null) {
                System.err.println("Failed to establish database connection.");
                return;
            }

            String request = Employee.getEmployeeInfos();
            try (PreparedStatement preparedStatement = connection.prepareStatement(request);
                 ResultSet resultSet = preparedStatement.executeQuery()) {

                System.out.println("Employee List:");
                System.out.println("| Name       | Surname         | Email                     | DOB             |");

                while (resultSet.next()) {
                    String name = resultSet.getString("Name");
                    String surname = resultSet.getString("Surname");
                    String email = resultSet.getString("Email");
                    java.sql.Date DOB = resultSet.getDate("DOB");
                    System.out.printf("| %-10s | %-15s | %-25s | %-15s |\n", name, surname, email, DOB);
                }
            } catch (SQLException ex) {
                System.err.println("Failed to execute SQL query: " + ex.getMessage());
            }
        } catch (SQLException ex) {
            System.err.println("Failed to establish database connection: " + ex.getMessage());
        }
    }
}
