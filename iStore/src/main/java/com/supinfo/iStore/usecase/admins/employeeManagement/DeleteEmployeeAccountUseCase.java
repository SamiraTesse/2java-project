package com.supinfo.iStore.usecase.admins.employeeManagement;

import com.supinfo.iStore.infrastructure.account.AccountDeletion;
import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.employee.Employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DeleteEmployeeAccountUseCase {
    public static void deleteEmployeeProfile(int ID) {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            if (connection == null) {
                System.err.println("Error 404. Something went wrong!");
                return;
            }
            String getEmployeeInfosQuery = Employee.getEmployeeById();
            try (PreparedStatement preparedStatement = connection.prepareStatement(getEmployeeInfosQuery)) {
                preparedStatement.setInt(1, ID);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        String email = resultSet.getString("email");
                        String deleteFromWhitelistQuery = AccountDeletion.getDeleteInfoQueryWhitelist();
                        try (PreparedStatement infoStatement = connection.prepareStatement(deleteFromWhitelistQuery)) {
                            infoStatement.setString(1, email);
                            infoStatement.executeUpdate();
                                System.out.println("Account infos deleted.");
                        }

                        String deleteFromIdentifiantsQuery = AccountDeletion.getDeleteCredentialsQuery();
                        try (PreparedStatement credentialsStatement = connection.prepareStatement(deleteFromIdentifiantsQuery)) {
                            credentialsStatement.setString(1, email);
                            credentialsStatement.executeUpdate();
                            System.out.println("Account credentials deleted.");
                        }

                        String deleteFromEmployeeQuery = Employee.getDeleteEmployeeQuery();
                        try (PreparedStatement credentialsStatement = connection.prepareStatement(deleteFromEmployeeQuery)) {
                            credentialsStatement.setString(1, email);
                            credentialsStatement.executeUpdate();
                            System.out.println("Employee has been deleted.");
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage() + '.');
        }
    }

}
