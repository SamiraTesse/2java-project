package com.supinfo.iStore.usecase.admins.store;

import com.supinfo.iStore.infrastructure.employee.AddEmployeeToStore;
import com.supinfo.iStore.infrastructure.database.DatabaseHandler;
import com.supinfo.iStore.infrastructure.store.StoreAccessList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StoreDisplayAccessList {
    public static void display(String storeName) {
        try (Connection connection = DatabaseHandler.establishConnection()) {
            String selectStoreQuery = AddEmployeeToStore.getSelectStoreIdQuery();
            assert connection != null;
            try (PreparedStatement selectStoreStatement = connection.prepareStatement(selectStoreQuery)) {
                selectStoreStatement.setString(1, storeName);
                try (ResultSet resultSet = selectStoreStatement.executeQuery()) {
                    if (resultSet.next()) {
                        int storeId = resultSet.getInt("id");
                        String selectEmployeesQuery = StoreAccessList.getSelectEmployeesQuery();
                        try (PreparedStatement selectEmployeesStatement = connection.prepareStatement(selectEmployeesQuery)) {
                            selectEmployeesStatement.setInt(1, storeId);
                            try (ResultSet employeesResultSet = selectEmployeesStatement.executeQuery()) {
                                List<String> employees = new ArrayList<>();
                                while (employeesResultSet.next()) {
                                    String employeeName = employeesResultSet.getString("name");
                                    String employeeSurname = employeesResultSet.getString("surname");
                                    String fullName = employeeName + " " + employeeSurname;
                                    employees.add(fullName);
                                }
                                System.out.println("People with access to store " + storeName + ":");
                                for (String employee : employees) {
                                    System.out.println(employee);
                                }
                            }

                        }
                    } else {
                        System.out.println("Store does not exist.");
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
    }

}
